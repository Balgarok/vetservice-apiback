const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;

var rendezvousSchema = new Schema({
    'date': {type: String, required:true, unique: true},
    'utilisateur': {
        type: Schema.Types.ObjectId,
        ref: 'Utilisateur'
    },
    'animal': {
        type: Schema.Types.ObjectId,
        ref: 'Animal'
    },
    'clinique': {
        type: Schema.Types.ObjectId,
        ref: 'Clinique'
    },
});

rendezvousSchema.plugin(uniqueValidator);

module.exports = mongoose.model('RendezVous', rendezvousSchema);