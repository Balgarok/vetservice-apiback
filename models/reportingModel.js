const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;

var reportingSchema = new Schema({
    'date' : {required: String},
    'page': {type: String, required:true},
    'description': {type: String, required:true} 
});

reportingSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Reporting', reportingSchema);