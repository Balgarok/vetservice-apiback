const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;

var animalSchema = new Schema({
    'nom': {type: String, required:true},
    'maitre': {
        type: Schema.Types.ObjectId,
        ref: 'Utilisateur'
    },
    'age': {type: String, required:true},
    'type': {type: String, required:true},
    'race': {type: String, required:true}
});

animalSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Animal', animalSchema);