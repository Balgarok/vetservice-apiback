const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;

var entreesCarnetSchema = new Schema({
    'date': {type: String, required:true, unique: true},
    'animal': {
        type: Schema.Types.ObjectId,
        ref: 'Animal'
    },
    'entree': {type: String, required:true}
});

entreesCarnetSchema.plugin(uniqueValidator);

module.exports = mongoose.model('EntreesCarnet', entreesCarnetSchema);