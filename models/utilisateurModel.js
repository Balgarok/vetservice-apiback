const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;

var utilisateurModelSchema = new Schema({
    'username': {type: String, required:true, unique: true},
    'firstname': {type: String, required:false},
    'lastname': {type: String, required:false},
    'email': {type: String, required:true, unique: true},
    'password': {type: String, required:true},
    'telephone': {type: String, required:true},
    'address': {type: String, required:true}
});

utilisateurModelSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Utilisateur', utilisateurModelSchema);