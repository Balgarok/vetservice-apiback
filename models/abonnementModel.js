const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;

var abonnementSchema = new Schema({
    'date': {type: String, required:true, unique:true},
    'clinique': {
        type: Schema.Types.ObjectId,
        ref: 'Clinique'
    },
    'nomAbonnement': {type: String, required:true},
    'prixAbonnement': {type: String, required:true}
});

abonnementSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Abonnement', abonnementSchema);