const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;

var adminSchema = new Schema({
    'username': {type: String, required:true, unique: true},
    'firstname': {type: String, required:false},
    'lastname': {type: String, required:false},
    'password': {type: String, required:true}
})

adminSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Administration', adminSchema);