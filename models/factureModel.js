const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;

var factureSchema = new Schema({
    'date' : {type: String, required:true, unique:true},
    'clinique':{
        type: Schema.Types.ObjectId,
        ref: 'Clinique'
    },
    'paiement': {type: String, required:true}
});

factureSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Facture', factureSchema);