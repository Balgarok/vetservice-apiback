const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;

var cliniqueSchema = new Schema({
    'nomClinique': {type:String, require:true, unique: true},
    'adresse': {type:String, required:true},
    'nomVet': {type: String, required: true},
    'specialite': {type: String, required: true},
    'telephone': {type: String, required:true},
    'password': {type: String, required:true},
    'horaires': {type: String, required:true},
    'accepte': {type: String, required: true},
    'dureeRdv': {type: String, required: true}
});

cliniqueSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Clinique', cliniqueSchema);