const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
const Schema = mongoose.Schema;

var abonnementTypeSchema = new Schema({
    'nom': {type: String, required:true, unique:true},
    'prix': {type: String, reuqired:true}
});

abonnementTypeSchema.plugin(uniqueValidator);

module.exports = mongoose.model('AbonnementType', abonnementTypeSchema);