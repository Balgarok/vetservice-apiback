var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const mongoose = require('mongoose');
const dotenv = require('dotenv').config();
const cors = require('cors');

mongoose.connect(process.env.DBADDRESS, {useNewUrlParser: true,useUnifiedTopology: true})
.then(()=>{console.log("Connexion a MongoDB reussie");})
.catch(()=>{console.log("Echec de la connexion a la BD");});

var indexRouter = require('./routes/index');
var accueilRouter = require('./routes/accueil');
var adminRouter = require('./routes/administration');
var animalRouter = require('./routes/animaux');
var rdvRouter = require('./routes/rendezvous');
var reportingRouter = require('./routes/reporting');
var subRouter = require('./routes/subscription');
var userRouter = require('./routes/utilisateurs');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'twig');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(cors());
app.use('/', indexRouter);
app.use('/accueil', accueilRouter);
app.use('/animal', animalRouter);
app.use('/administration', adminRouter);
app.use('/rendezvous', rdvRouter);
app.use('/bug', reportingRouter);
app.use('/subs', subRouter);
app.use('/user', userRouter);

// ouverture de l'api aux utilisateurs exterieurs
app.use((req, res, next)=>{
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content, Accept, Content-Type, Authorization');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
  next();
})

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
