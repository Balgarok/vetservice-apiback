const entreeCarnetModel = require('../models/entreesCarnetModel');
const animalModel = require('../models/animauxModel');

module.exports = {
    afficheCarnet: async(req, res)=>{
        const id = req.params.id;
        entreeCarnetModel.find(({_id: id}), (err, entrees)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when retrieving animal carnet'
                });
            }
            if(!entrees){
                return res.status(404).json({
                    staus: 404,
                    message: 'No entries found'
                });
            }
            return res.status(200).json({
                status: 200,
                result: entrees
            });
        });
    },
    afficheCarnetClinique: async(req, res)=>{
        const id = req.params.id;
        entreeCarnetModel.find(({_id: id}), (err, entrees)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when retrieving animal carnet'
                });
            }
            if(!entrees){
                return res.status(404).json({
                    staus: 404,
                    message: 'No entries found'
                });
            }
            return res.status(200).json({
                status: 200,
                result: entrees
            });
        });
    },
    ajoutEntree: async(req, res)=>{
        var nouvelleEntree = new entreeCarnetModel({
            date: req.body.date,
            animal: req.body.animal,
            entree: req.body.entree
        });
        await nouvelleEntree.save((err, newEntree)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when saving entry'
                });
            }
            return res.status(201).json({
                status: 201,
                message: 'Entry created'
            });
        });
    },
    infoAnimal: async(req, res)=>{
        const id = req.params.id;
        animalModel.findOne(({_id: id}), (err, animal)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when retrieving animal'
                });
            }
            if(!animal){
                return res.status(404).json({
                    staus: 404,
                    message: 'No animal found'
                });
            }
            return res.status(200).json({
                status: 200,
                result: animal
            });
        });
    },
    creationAnimal: async(req, res)=>{
        var nouvelAnimal = new animalModel ({
            nom: req.body.nom,
            maitre: req.body.maitre,
            age: req.body.age,
            type: req.body.type,
            race: req.body.race
        });
        await nouvelAnimal.save((err, newAnimal)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when saving animal'
                });
            }
            return res.status(201).json({
                status: 201,
                message: 'Animal created'
            });
        });
    },
    listAnimaux: async(req, res)=>{
        const id = req.params.id;
        animalModel.find(({maitre: id}), (err, animaux)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when retrieving animal'
                });
            }
            if(!animaux){
                return res.status(404).json({
                    staus: 404,
                    message: 'No animal found'
                });
            }
            return res.status(200).json({
                status: 200,
                result: animaux
            });
        });
    }
}