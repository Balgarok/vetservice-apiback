const subsModel = require('../models/abonnementModel');

module.exports = {
    subsClinique: async (req, res)=>{
        const id = req.params.id;
        subsModel.findOne({_id: id}, (err, subs)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when retrieving the subs'
                });
            }
            if(!subs){
                return res.status(404).json({
                    status: 404,
                    message: 'No subs found'
                });
            }
            return res.status(200).json({
                status: 200,
                result: subs
            });
        });
    },
    listSubsClinique: async (req, res)=>{
        subsModel.find(({}),(err, subs)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when retrieving all subs'
                });
            }
            if(!subs){
                return res.status(404).json({
                    status: 404,
                    message: 'No subs found'
                });
            }
            return res.status(200).json({
                status: 200,
                result: subs
            });
        });
    },
    envoiSubsClinique: async (req, res)=>{
        var nouveauAbonnement = new subsModel({
            date: req.body.date,
            clinique: req.body.clinique,
            nomAbonnement: req.body.nomAbonnement,
            prixAbonnement: req.body.prixAbonnement
        });
        await nouveauAbonnement.save((err, subs)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when saving subs report'
                });
            }
            return res.status(201).json({
                status: 201,
                message: 'Subs report created'
            });
        });
    },
}