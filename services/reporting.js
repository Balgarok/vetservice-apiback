const bugModel = require('../models/reportingModel');

module.exports = {
    reporting: async(req, res)=>{
        var nouveauBug = new bugModel({
            date: req.body.date,
            page: req.body.page,
            description: req.body.description
        });
        await nouveauBug.save((err, bug)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when saving bug report'
                });
            }
            return res.status(201).json({
                status: 201,
                message: 'Bug report created'
            });
        });
    },
    getReporting: async(req, res)=>{
        const id = req.params.id;
        bugModel.findOne({_id: id}, (err, bug)=>{
            if(err){
                return res.status(500).jsqon({
                    status: 500,
                    message: 'Error when retrieving the bug'
                });
            }
            if(!bug){
                return res.status(404).json({
                    status: 404,
                    message: 'No bug found'
                });
            }
            return res.status(200).json({
                status: 200,
                result: bug
            });
        });
    },
    listReporting: async(req, res)=>{
        bugModel.find(({}), (err, bugs)=>{
            if(err){
                return res.status(500).jsqon({
                    status: 500,
                    message: 'Error when retrieving the bug'
                });
            }
            if(!bugs){
                return res.status(404).json({
                    status: 404,
                    message: 'No bug found'
                });
            }
            return res.status(200).json({
                status: 200,
                result: bugs
            });
        });
    }
}