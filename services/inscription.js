const jsonwebtoken = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const utilisateurModel = require('../models/utilisateurModel');
const cliniquerModel = require('../models/cliniqueModel');


module.exports = {
    signupUtilisateur : async (req, res)=>{
        bcrypt.hash(req.body.password, 10, (err, hash)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: err.message
                });
            }
            else{
                const newUtilisateur = new utilisateurModel({
                    username: req.body.username,
                    firstname: req.body.firstname,
                    lastname: req.body.lastename,
                    email: req.body.email,
                    address: req.body.address,
                    telephone: req.body.telephone,
                    password: hash,
                });
                newUtilisateur.save((err, utilisateur)=>{
                    if(err){
                        return res.status(400).json({
                            status: 400,
                            message: err.message
                        });
                    }
                    return res.status(201).json({
                        status: 201,
                        message: 'Utilisateur created'
                    });
                })
            }
        });
    },
    signupClinique : async (req, res)=>{
        bcrypt.hash(req.body.password, 10, (err, hash)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: err.message
                });
            }
            else{
                const newClinique = new cliniqueModel({
                    nomClinique: req.body.nomClinique,
                    nomVet: req.body.nomVet,
                    specialite: req.body.specialite,
                    email: req.body.email,
                    adresse: req.body.adresse,
                    telephone: req.body.telephone,
                    horaires: req.body.horaires,
                    dureeRdv: req.body.dureeRdv,
                    accepte: "non",
                    password: hash,
                });
                newClinique.save((err, clinique)=>{
                    if(err){
                        return res.status(400).json({
                            status: 400,
                            message: err.message
                        });
                    }
                    return res.status(201).json({
                        status: 201,
                        message: 'Clinique created'
                    });
                })
            }
        });
    },
    validInscription : async (req, res)=>{
        const id = req.params.id;
        clientModel.findOne({_id: id}, (err, clinique)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when retrieving clinique'
                });
            }
            if(!clinique){
                return res.status(404).json({
                    status: 404,
                    message: 'Clinique not found'
                });
            }
            clinique.updateOne({accepte: 'oui'},(err, clinique)=>{
                if(err){
                    return res.status(500).json({
                        status: 500,
                        message: 'Error when retrieving clinique'
                    });
                }
                return res.status(200).json({
                    status: 200,
                    message: 'Clinique profile modified'
                });
            });
        });

    }
}