const cliniqueModel = require('../models/cliniqueModel');
const utilisateurModel = require('../models/utilisateurModel');
const jsonwebtoken = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const administrationModel = require('../models/administrationModel');

module.exports = {
    loginUtilisateur: async (req, res) =>{
        utilisateurModel.findOne({username: req.body.username}, (err, user)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: err.message
                });
            }
            if(!user){
                return res.status(404).json({
                    status: 404,
                    message: 'User not found'
                });
            }
            bcrypt.compare(req.body.password, user.password, (err, verif)=>{
                if(err){
                    return res.status(500).json({
                        status: 500,
                        message: err.message
                    });
                }
                if(!verif){
                    return res.status(401).json({
                        status: 401,
                        message: 'Wrong password'
                    });
                }
                return res.status(200).json({
                    userId: user._id,
                    token: jsonwebtoken.sign(
                        {userId: user._id},
                        process.env.SECRET,
                        {expiresIn: '24h'}
                    )
                });
            });
        });
    },
    loginClinique: async (req, res) =>{
        cliniqueModel.findOne({nomClinique: req.body.nomClinique}, (err, user)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: err.message
                });
            }
            if(!user){
                return res.status(404).json({
                    status: 404,
                    message: 'Clinique not found'
                });
            }
            bcrypt.compare(req.body.password, user.password, (err, verif)=>{
                if(err){
                    return res.status(500).json({
                        status: 500,
                        message: err.message
                    });
                }
                if(!verif){
                    return res.status(401).json({
                        status: 401,
                        message: 'Wrong password'
                    });
                }
                return res.status(200).json({
                    userId: user._id,
                    token: jsonwebtoken.sign(
                        {userId: client._id},
                        process.env.SECRET,
                        {expiresIn: '24h'}
                    )
                });
            });
        });
    },
    loginAdmin: async (req, res) =>{
        administrationModel.findOne({username: req.body.username}, (err, user)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: err.message
                });
            }
            if(!user){
                return res.status(404).json({
                    status: 404,
                    message: 'Admin not found'
                });
            }
            bcrypt.compare(req.body.password, user.password, (err, verif)=>{
                if(err){
                    return res.status(500).json({
                        status: 500,
                        message: err.message
                    });
                }
                if(!verif){
                    return res.status(401).json({
                        status: 401,
                        message: 'Wrong password'
                    });
                }
                return res.status(200).json({
                    userId: user._id,
                    token: jsonwebtoken.sign(
                        {userId: client._id},
                        process.env.SECRET,
                        {expiresIn: '24h'}
                    )
                });
            });
        });
    }
}
