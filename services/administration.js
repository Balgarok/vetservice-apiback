const factureModel = require('../models/factureModel');
const cliniqueModel = require('../models/cliniqueModel');

module.exports = {
    listFactures: async (req, res)=>{
        factureModel.find(({}),(err, factures)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when retrieving all factures'
                });
            }
            if(!factures){
                return res.status(404).json({
                    status: 404,
                    message: 'No factures found'
                });
            }
            return res.status(200).json({
                status: 200,
                result: factures
            });
        });
    },
    affFacture: async (req, res)=>{
        const id = req.params.id;
        factureModel.findOne({_id: id}, (err, facture)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when retrieving the facture'
                });
            }
            if(!facture){
                return res.status(404).json({
                    status: 404,
                    message: 'No facture found'
                });
            }
            return res.status(200).json({
                status: 200,
                result: facture
            });
        });
    },
    listDemandesInscription: async(req, res)=>{
        cliniqueModel.find({accepte:'non'}, (err, cliniques)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when retrieving all cliniques demands'
                });
            }
            if(!cliniques){
                return res.status(404).json({
                    status: 404,
                    message: 'No cliniques found'
                });
            }
            return res.status(200).json({
                status: 200,
                result: cliniques
            });
        });
    },
    affDemandeInscription: async (req, res)=>{
        const id = req.params.id;
        cliniqueModel.findOne({_id: id}, (err, clinique)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when retrieving the clinique'
                });
            }
            if(!clinique){
                return res.status(404).json({
                    status: 404,
                    message: 'No clinique found'
                });
            }
            return res.status(200).json({
                status: 200,
                result: clinique
            });
        });
    },
    refuInscription: async (req, res)=>{
        const id = req.params.id;
        cliniqueModel.findByIdAndRemove(id, (err, clinique)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when deleting the clinique'
                });
            }
            if(!clinique){
                return res.status(404).json({
                    status: 404,
                    message: 'No clinique found'
                });
            }
            return res.status(204).json();
        });
    }
}