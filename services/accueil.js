const clientModel = require('../models/utilisateurModel');
const cliniqueModel = require('../models/cliniqueModel');

module.exports = {
    accueilClient: async(req, res)=>{
        const id = req.params.id;
        clientModel.findOne(({_id: id}), (err, user)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when retrieving user'
                });
            }
            if(!user){
                return res.status(404).json({
                    staus: 404,
                    message: 'No user found'
                });
            }
            return res.status(200).json({
                status: 200,
                result: user
            });
        });
    },
    accueilClinique: async(req, res)=>{
        const id = req.params.id;
        cliniqueModel.findOne(({_id: id}), (err, clinique)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when retrieving clinique'
                });
            }
            if(!clinique){
                return res.status(404).json({
                    staus: 404,
                    message: 'No clinique found'
                });
            }
            return res.status(200).json({
                status: 200,
                result: clinique
            });
        });
    }
}