const rdvModel = require('../models/rendezvousModel');

module.exports = {
    priserdv: async(req, res)=>{
        const id = req.params.id;
        var nouveaurdv = new rdvModel({
            date: req.body.date,
            utilisateur: id,
            animal: req.body.animal,
            clinique: req.body.clinique
        });
        await nouveauBug.save((err, rdv)=>{
            if(err){
                return res.status(500).json({
                    status: 500,
                    message: 'Error when saving rendez vous'
                });
            }
            return res.status(201).json({
                status: 201,
                message: 'Rendez vous created'
            });
        });
    },
    supprrdv : async(req, res)=>{
        const id = req.params.id;
        rdvModel.findByIdAndRemove(id, (err, rdv)=>{
            if(err){
                return res.status(500).jsqon({
                    status: 500,
                    message: 'Error when deleting the rendez vous'
                });
            }
            if(!rdv){
                return res.status(404).json({
                    status: 404,
                    message: 'No rendez vous found'
                });
            }
            return res.status(204).json();
        });
    },
    infoRdv: async(req, res)=>{
        const id = req.params.id;
        rdvModel.findOne({_id: id}, (err, rdv)=>{
            if(err){
                return res.status(500).jsqon({
                    status: 500,
                    message: 'Error when retrieving the rdv'
                });
            }
            if(!rdv){
                return res.status(404).json({
                    status: 404,
                    message: 'No rdv found'
                });
            }
            return res.status(200).json({
                status: 200,
                result: rdv
            });
        });
    },
    listClin: async(req, res)=>{
        const id = req.params.id;
        rdvModel.find({clinique: id}, (err, rdv)=>{
            if(err){
                return res.status(500).jsqon({
                    status: 500,
                    message: 'Error when retrieving the rdv'
                });
            }
            if(!rdv){
                return res.status(404).json({
                    status: 404,
                    message: 'No rdv found'
                });
            }
            return res.status(200).json({
                status: 200,
                result: rdv
            });
        });
    }
}