const animauxInfoService = require('../services/animauxInformations');

module.exports= {
    carnetClient: async(req, res)=>{
        await animauxInfoService.afficheCarnet(req, res);
    },
    carnetClinique: async(req, res)=>{
        await animauxInfoService.afficheCarnetClinique(req, res);
    },
    modificationCarnet: async(req, res)=>{
        await animauxInfoService.ajoutEntree(req, res);
    },
    suppressionCarnet: async(req, res)=>{
        /**suppression du carnet de santé de l'animal faire attention à ce que le client soit bien le maitre de l'animal */
    },
    infoAnimal: async(req, res)=>{
        await animauxInfoService.infoAnimal(req, res);
    },
    creationAnimal: async(req, res)=>{
        await animauxInfoService.creationAnimal(req, res);
    },
    listAnimaux: async(req, res)=>{
        await animauxInfoService.listAnimaux(req, res);
    },
}