const connexionService = require('../services/connexion');

module.exports = {
    loginClient: async(req, res)=>{
        await connexionService.loginUtilisateur(req, res);
    },
    loginClinique: async(req, res)=>{
        await connexionService.loginClinique(req, res);
    },
    loginAdministration: async(req, res)=>{
        await connexionService.loginAdmin(req, res);
    }
}