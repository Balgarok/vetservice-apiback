const inscriptionService = require('../services/inscription');
const adminService = require('../services/administration');

module.exports = {
    factures : async(req, res)=>{
        await adminService.listFactures(req, res);
    },
    affFacture : async(req, res)=>{
        await adminService.affFacture(req, res);
    },
    demandesInscription : async(req, res)=>{
        await adminService.listDemandesInscription(req, res);
    },
    affDemandeIncription : async(req ,res)=>{
        await adminService.affDemandeInscription(req, res);
    },
    validInscription : async(req, res)=>{
        await inscriptionService.validInscription(req, res);
    },
    refuInscription : async(req, res)=>{
        await adminService.refuInscription(req, res);
    }
}