const accueilService = require('../services/accueil');

module.exports = {
    accueilClient : async(req, res)=>{
        await accueilService.accueilClient(req, res);
    },
    accueilClinique : async(req, res)=>{
        await accueilService.accueilClinique(req, res);
    }
}