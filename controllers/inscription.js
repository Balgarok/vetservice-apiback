const inscriptionService = require('../services/inscription');

module.exports = {
    utilisateur: async(req, res)=>{
        await inscriptionService.signupUtilisateur(req, res);
    },
    clinique: async(req, res)=>{
        await inscriptionService.signupClinique(req, res);
    },
}