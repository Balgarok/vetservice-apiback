const accueilService = require('../services/accueil');

module.exports = {
    updateClient: async(req,res)=>{
        /**changement des infos client */
    },
    updateClinique: async(req, res)=>{
        /** changement des infos clinique */
    },
    clientToClientInfo: async(req, res)=>{
        await accueilService.accueilClient(req, res);
    },
    cliniqueToClientInfo: async(req, res)=>{
        await accueilService.accueilClinique(req, res);
    },
    cliniqueToClinique: async(req,res)=>{
        await accueilService.accueilClinique(req, res);
    },
    cliniqueToAdminInfo: async(req, res)=>{
        await accueilService.accueilClinique(req, res);
    }
}