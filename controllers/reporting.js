const reportingService = require('../services/reporting');

module.exports = {
    reporting : async(req, res)=>{
        await reportingService.reporting(req, res);
    },
    getReporting : async(req, res)=>{
        await reportingService.getReporting(req, res);
    },
    listReporting : async(req, res)=>{
        await reportingService.listReporting(req, res);
    }
}