const rdvService = require('../services/rendezvous');

module.exports = {
    priseRdv : async(req, res)=>{
        await rdvService.priserdv(req, res);
    },
    supprRdv : async (req, res)=>{
        await rdvService.supprrdv(req, res);
    },
    infoRdv : async(req, res)=>{
        await rdvService.infoRdv(req, res);
    },
    infoRdvVet : async(req, res)=>{
        await rdvService.infoRdv(req, res);
    },
    supprRdvVet: async(req, res)=>{
        await rdvService.supprrdv(req, res);
    },
    listClin: async(req, res)=>{
        await rdvService.listClin(req, res);
    }
}