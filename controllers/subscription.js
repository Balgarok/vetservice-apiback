const subsService = require('../services/subs');

module.exports ={
    subsClinique : async(req, res)=>{
        await subsService.subsClinique(req, res);
    },
    listSubsClinique : async(req, res)=>{
        await subsService.listSubsClinique(req, res);
    },
    envoiSubsClinique : async(req, res)=>{
        await subsService.envoiSubsClinique(req, res);
    },
    subsCliniqueToAdmin : async(req, res)=>{
        await subsService.subsClinique(req, res);
    }
}