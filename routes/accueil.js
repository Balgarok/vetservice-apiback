var express = require('express');
var accueilController = require('../controllers/accueil');
var auth = require('../middlewares/auth');
var authClin = require('../middlewares/authClin');
var router = express.Router();

router.get('/accueilClient/:id', auth, accueilController.accueilClient);

router.get('/accueilClini:/id', authClin, accueilController.accueilClinique);

module.exports = router;