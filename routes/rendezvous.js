var express = require('express');
var rendezVousController = require('../controllers/rendezvous');
var auth = require('../middlewares/auth');
var authClin = require('../middlewares/authClin');
var router = express.Router();

router.post('/priseRdv/:id', auth, rendezVousController.priseRdv);

router.delete('/:id', auth, rendezVousController.supprRdv);

router.get('/:id', auth, rendezVousController.infoRdv);

router.get('/rdvMed/:id', authClin, rendezVousController.infoRdvVet);

router.delete('/rdvMed/:id', authClin, rendezVousController.supprRdvVet);

router.get('/rdvClin/:id', authClin, rendezVousController.listClin);

module.exports = router;