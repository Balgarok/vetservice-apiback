var express = require('express');
var auth = require('../middlewares/auth');
var authClin = require('../middlewares/authClin');
var animauxController = require('../controllers/animauxInfos');
var router = express.Router();

router.post('/animal', auth, animauxController.creationAnimal);

router.get('/entrees/:id', auth, animauxController.carnetClient);

router.get('/animalClin/:id', authClin, animauxController.carnetClinique);

router.put('/animalClin/:id', authClin, animauxController.modificationCarnet);

router.delete('/animal/:id', auth, animauxController.suppressionCarnet);

router.get('/animaux/:id', auth, animauxController.listAnimaux);

router.get('/animalById/:id', auth, animauxController.infoAnimal);

module.exports = router;