var express = require('express');
var subController = require('../controllers/subscription');
var authClin = require('../middlewares/authClin');
var authAdmin = require('../middlewares/authAdm');
var router = express.Router();

router.get('/subscriptionClini/:id', authClin, subController.subsClinique);

router.get('/subscription', authAdmin, subController.listSubsClinique);

router.post('/envoieSubClin/:id', authClin, subController.envoiSubsClinique);

router.get('/subsCliniqueAdmin/:id', authAdmin, subController.subsCliniqueToAdmin);

module.exports = router;

