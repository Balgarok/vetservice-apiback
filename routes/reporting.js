var express = require('express');
var reportingController = require('../controllers/reporting');
const authAdm = require('../middlewares/authAdm');
var router = express.Router();

router.post('/reporting', reportingController.reporting);

router.get('/reporting/:id', authAdm, reportingController.getReporting);

router.get('/reporting', authAdm, reportingController.listReporting);

module.exports = router;