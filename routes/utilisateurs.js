var express = require('express');
var auth = require('../middlewares/auth');
var authAdm = require('../middlewares/authAdm');
var authClin = require('../middlewares/authClin');
var inscriptionController = require('../controllers/inscription');
var loginController = require('../controllers/connexion');
var infosController = require('../controllers/utilisateursInfo');
var router = express.Router();

router.post('/signup', inscriptionController.utilisateur);

router.post('/signupCli', inscriptionController.clinique);

router.post('/login', loginController.loginClient);

router.post('/loginAdm', loginController.loginAdministration);

router.post('loginClin', loginController.loginClinique);

router.put('/update', auth, infosController.updateClient);

router.put('/updateClin', authClin, infosController.updateClinique);

router.get('/infos/:id', auth, infosController.clientToClientInfo);

router.get('infosClin/:id', auth, infosController.cliniqueToClientInfo);

router.get('/infosClinMed/:id', authClin, infosController.cliniqueToClinique);

router.get('/infosClinAdm/:id', authAdm, infosController.cliniqueToAdminInfo);

module.exports = router;