var express = require('express');
var adminController = require('../controllers/administration');
var authAdm = require('../middlewares/authAdm');
var router = express.Router();

router.get('/factures', authAdm, adminController.factures);

router.get('/facture/:id', authAdm, adminController.affFacture);

router.get('/demandesInscription', authAdm, adminController.demandesInscription);

router.get('/demandeInsciption/:id', authAdm, adminController.affDemandeIncription);

router.post('/validationInscription/:id', authAdm, adminController.validInscription);

router.post('refusInscription/:id', authAdm, adminController.refuInscription);

module.exports = router;